var express = require('express'),
    app = express();

var env = process.env.NODE_ENV || 'development';

var config = require('./server/config/config')[env];
require('./server/config/express')(app, config);
require('./server/config/mongoose')(config);
require('./server/config/routes')(app);

app.listen(config.port, function () {
    console.log('Listening on port '+ config.port + '...')
});