console.log('mainCtrl.js loaded');

var app = angular.module('recovery-systems', ['ngGrid', 'angularFileUpload']);
app.controller("mainCtrl", function ($scope, $http, $upload) {
    $scope.myData = [];
    $scope.gridOptions = {
        data: 'myData',
        columnDefs: [{field:'name', displayName:'Name'},
                     {field:'lastModified', displayName:'Last Modified'},
                     {field:'size', displayName:'Size'},
                     {field:'delete', displayName:'Delete', cellTemplate: '<button ng-click="deleteFile(row.entity.name)" confirmation-needed="Really delete file {{row.entity.name}}?">Delete</button>'}]
    };
    $scope.$watch('myData', function(newVal, oldVal){
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }, true);
    $scope.onFileSelect = function (){
        var file = document.getElementById('uploadFile').files[0];
        if(file){
            $scope.upload = $upload.upload({
                url: '/upload',
                data: {},
                file: file
            }).progress(function(evt) {
                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function(data, status, headers, config) {
                console.log(data);
                alert(data.reason);
                document.getElementById('uploadFile').value='';
                $scope.filelist();
            }).error(function(data, status, headers, config){
                console.log(data);
                alert(data.reason);
                document.getElementById('uploadFile').value=''
            });
        }
    };
    $scope.filelist = function (){
        $http.get('/filelist').success(function (data) {
            console.log(data);
            $scope.myData = data;
        }).error(function (data) {
            console.error(data);
        });
    };
    $scope.deleteFile = function (fileName){
        $http.delete('/delete/' + fileName).success(function (data) {
            console.log(data);
            $scope.filelist();
        }).error(function (data) {
            console.error(data);
        });
    };
    $scope.filelist();
});
app.directive('confirmationNeeded', function () {
    return {
        priority: 1,
        terminal: true,
        link: function (scope, element, attr) {
            var msg = attr.confirmationNeeded || "Are you sure?";
            var clickAction = attr.ngClick;
            element.bind('click',function () {
                if ( window.confirm(msg) ) {
                    scope.$eval(clickAction)
                }
            });
        }
    };
});