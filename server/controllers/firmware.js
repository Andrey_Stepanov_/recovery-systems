var fs = require('fs');

var File = require('mongoose').model('File'),
    Info = require('mongoose').model('Info');

exports.upload = function (files, res) {
    Info.find({name: files.file.name}).exec(function (err, info) {
        if (err) {
            res.status(400).send({reason: err.toString()})
        } else {
            if (info && info.length > 0) {
                res.status(409).send({reason: 'File with such name already exists'});
            } else {
                fs.readFile(files.file.path, function (err, data) {
                    if (err) {
                        res.status(400).send({reason: err.toString()})
                    } else {
                        var file = new File({content: data.toString('base64')});
                        file.save(function (err, file) {
                            if (err) {
                                res.status(400).send({reason: err.toString()})
                            } else {
                                var infoData = {fileId: file._id,
                                    name: files.file.name,
                                    path: files.file.path,
                                    type: files.file.type,
                                    size: files.file.size};
                                var info = new Info(infoData);
                                info.save(function (err) {
                                    if (err) {
                                        res.status(400).send({reason: err.toString()})
                                    } else {
                                        res.status(200).send({reason: 'File uploaded'});
                                    }
                                })
                            }
                        });
                    }
                });
            }
        }
    })
};

exports.download = function (name, res) {
    Info.findOne({name: name}).exec(function (err, info) {
        if (err) {
            res.status(400).send({reason: err.toString()})
        } else {
            if (info) {
                File.findOne({_id: info.fileId}).exec(function (err, file) {
                    if (err) {
                        res.status(400).send({reason: err.toString()})
                    } else {
                        if (file) {
                            res.status(200).send(file.content)
                        } else {
                            res.status(404).send({reason: 'Such file not found'})
                        }
                    }
                })
            } else {
                res.status(404).send({reason: 'Such file not found'})
            }
        }
    });
};

exports.delete = function (name, res) {
    Info.findOne({name: name}).exec(function (err, info) {
        if (err) {
            res.status(400).send({reason: err.toString()})
        } else {
            if (info) {
                Info.remove({name: name}).exec(function (err) {
                    if (err) {
                        res.status(400).send({reason: err.toString()})
                    } else {
                        File.remove({_id: info.fileId}).exec(function (err) {
                            if (err) {
                                res.status(400).send({reason: err.toString()})
                            } else {
                                res.status(200).send({reason: 'File removed'})
                            }
                        });
                    }
                });
            } else {
                res.status(404).send({reason: 'Such file not found'})
            }
        }
    });
};

exports.getFileList = function (req, res) {
    Info.find({}).exec(function (err, collection) {
        if (err) {
            res.status(400).send({reason: err.toString()})
        } else {
            res.send(collection);
        }
    })
};