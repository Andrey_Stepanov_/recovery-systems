var mongoose = require('mongoose');

var infoSchema = mongoose.Schema({
    fileId: String,
    name: String,
    path: String,
    type: String,
    size: Number,
    lastModified: { type: Date, default: Date.now }
});

var Info = mongoose.model('Info', infoSchema);

function createDefaultInfos() {
    //TODO: not sure how to do this w/out the reference to order_id
}

exports.createDefaultInfos = createDefaultInfos;
