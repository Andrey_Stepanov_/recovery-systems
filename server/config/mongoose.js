var mongoose = require('mongoose'),
    fileModel = require('../models/File'),
    infoModel = require('../models/Info');

module.exports = function(config) {
    mongoose.connect(config.db);

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'Connection error... '));
    db.once('open', function callback() {
        console.log('Recovery Systems db opened');
    });

    fileModel.createDefaultFiles();
    infoModel.createDefaultInfos()
};