var firmware = require('../controllers/firmware');

var multipart = require('connect-multiparty'),
    multipartMiddleware = multipart();

module.exports = function(app) {
    app.post('/upload', multipartMiddleware, function(req, res) {
        firmware.upload(req.files, res)
    });

    app.get('/firmware/:name', function(req, res) {
        firmware.download(req.params.name, res)
    });

    app.delete('/delete/:name', function(req, res) {
        firmware.delete(req.params.name, res)
    });

    app.get('/filelist', function(req, res) {
        firmware.getFileList(req, res)
    });

    app.get('/', function(req, res) {
        res.render('index.jade')
    });

    app.get('*', function(req, res){
        res.redirect('/')
    });
};