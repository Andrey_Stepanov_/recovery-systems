var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');

module.exports = {
    development: {
        db: 'mongodb://admin:1q2w3e4r5t6y@kahana.mongohq.com:10094/app29101615',
        rootPath: rootPath,
        port: process.env.PORT || 3000
    },
    production: {
        db: 'mongodb://admin:1q2w3e4r5t6y@kahana.mongohq.com:10094/app29101615',
        rootPath: rootPath,
        port: process.env.PORT || 80
    }
};